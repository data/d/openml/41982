# OpenML dataset: Kuzushiji-MNIST

https://www.openml.org/d/41982

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Much of machine learning research focuses on producing models which perform well on benchmark tasks, in turn improving our understanding of the challenges associated with those tasks. From the perspective of ML researchers, the content of the task itself is largely irrelevant, and thus there have increasingly been calls for benchmark tasks to more heavily focus on problems which are of social or cultural relevance. In this work, we introduce Kuzushiji-MNIST, a dataset which focuses on Kuzushiji (cursive Japanese), as well as two larger, more challenging datasets, Kuzushiji-49 and Kuzushiji-Kanji. Through these datasets, we wish to engage themachine learning community into the world of classical Japanese literature.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41982) of an [OpenML dataset](https://www.openml.org/d/41982). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41982/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41982/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41982/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

